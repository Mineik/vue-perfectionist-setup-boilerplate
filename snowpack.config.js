/** @type {import("snowpack").SnowpackUserConfig } */
module.exports = {
	plugins: [
		["./src/routeGenerator.ts",{
			path: "src/pages",
			outPath: "src/router/routes.js",
			importPrefix: "../pages/"
		}],
		"@snowpack/plugin-vue",
		"@snowpack/plugin-sass",
		"@snowpack/plugin-typescript",
		"@snowpack/plugin-webpack",
	],
	devOptions: {
		hostname: "localhost",
		port: 3000,
		open:"none",
	},
	mount:{
		"public": {url:"/", static:true},
		"src": {url:"/dist"}
	},
	routes:[
		{
			match: "routes",
			src: ".*",
			dest:"/index.html"
		}
	]

}