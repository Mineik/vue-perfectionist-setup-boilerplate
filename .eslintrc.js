module.exports = {
	root: true,
	parser: "@typescript-eslint/parser",
	extends: [
		"plugin:vue/recommended"
	],
	plugins: [
		"@typescript-eslint",
		"vue"
	],
	rules:{
		"vue/html-indent":[2,"tab"],
		"vue/script-indent":[2,"tab"],
		"@typescript-eslint/indent":[2,"tab"],
		"vue/match-component-file-name":[2,{
			extensions:["vue"],
			shouldMatchCase: false
		}],
		indent: [2, "tab"],
		"no-tabs": "off",
		quotes: "off",
		eqeqeq: "off"

	}
}