/**
 this plugin was made for the sole purpose of sending death threats toward retarded devs who think creating low level libraries for frontend applications
 is a great idea
 */
const {generateRoutes} = require("vue-route-generator")
const {join} = require("path")
const {writeFileSync} = require("fs")
//@ts-ignore
module.exports = function (snowpackC, pluginOptions){
	return{
		name: "vue-auto-routing-snowpack",
		config(){
			const routes = generateRoutes({
				pages: join(process.cwd(), pluginOptions.path),
				importPrefix: pluginOptions.importPrefix? pluginOptions.importPrefix:pluginOptions.path+"/",
				chunkNamePrefix: pluginOptions.chunkNamePrefix? pluginOptions.chunkNamePrefix:undefined
			})
			writeFileSync(join(process.cwd(), pluginOptions.outPath), routes)
		}
	}
}