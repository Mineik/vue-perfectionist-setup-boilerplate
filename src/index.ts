import {createApp} from "vue"
import App from "./App.vue"
import {router} from "./router"

console.log("Mounting Vue.js...")

const app = createApp(App)
app.use(router)

app.mount("#app")
console.log("Vue successfully mounted!")