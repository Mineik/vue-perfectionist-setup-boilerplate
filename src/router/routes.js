function index() {
	return import(/* webpackChunkName: "index" */ '../pages/index.vue')
}
function about() {
	return import(/* webpackChunkName: "about" */ '../pages/about.vue')
}

export default [
	{
		name: 'index',
		path: '/',
		component: index,
	},
	{
		name: 'about',
		path: '/about',
		component: about,
	},
]
